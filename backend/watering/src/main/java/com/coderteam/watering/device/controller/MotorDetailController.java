package com.coderteam.watering.device.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import com.coderteam.watering.device.entity.MotorStatus;
import com.coderteam.watering.device.repos.MotorStatusRepos;
import lombok.Data;
@RestController
@RequestMapping("/motordetail")
public class MotorDetailController {

    private final MotorStatusRepos  repos;

    public MotorDetailController(MotorStatusRepos repos) {
        this.repos = repos;
    }

    @GetMapping("/list")
    public List<MotorStatus> getList() {
        return repos.findAll();
    }

    @PostMapping("/listValue")
    public List<MotorStatus> getListValue(@RequestBody DeviceId id)
    {
        List<MotorStatus> listMotor = repos.findAll();
        return listMotor.stream().filter(item -> item.getMotor().getDeviceId().equals(id.getDeviceId()))
                .collect(Collectors.toList());
    }
    
}
// @Data
// class DeviceMotorId {
//     public String Id;
// }