package com.coderteam.watering.device.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import com.coderteam.watering.device.entity.Motor;
import com.coderteam.watering.device.repos.MotorRepos;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/motor")
public class MotorController {

    private final MotorRepos repos;

    public MotorController(final MotorRepos repos) {
        this.repos = repos;
    }

    @GetMapping("/list")
    public List<Motor> getList() {
        return repos.findAll();
    }

    @PostMapping("/getMotorDefault")
    public List<Motor> getMotorDefault(@RequestBody DeviceId id)
    {
        List<Motor> listMotor = repos.findAll();
        System.out.println(id.getDeviceId());
        return listMotor.stream().filter(item -> item.getDeviceId().equals(id.getDeviceId()))
                .collect(Collectors.toList());
    }

    @PostMapping("/add")
    public Motor addMotor(@RequestBody String deviceId) {
        deviceId = deviceId.substring(0, deviceId.length() - 1);
        Optional<Motor> motorFind = repos.findByDeviceId(deviceId);
        if (!motorFind.isEmpty()) {
            return null;
        }
        Motor info = Motor.builder().deviceId(deviceId).build();
        repos.save(info);
        return info;
    }

}
