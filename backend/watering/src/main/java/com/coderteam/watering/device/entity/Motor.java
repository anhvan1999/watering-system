package com.coderteam.watering.device.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Motor {

    @Setter(value = AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String deviceId;

    @Column
    @Builder.Default
    private Short currentValue = (short)0;

    @Column
    @Builder.Default
    private Short lowerSensorBound = (short)0;

    @Column
    @Builder.Default
    private Short upperSensorBound = (short)1024;

    @Column
    @Builder.Default
    private Short autoControlValue = 500;

}
