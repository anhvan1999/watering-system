import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import store from '../redux-store/store';
import {
  takeDataSensor
} from '../redux-store/actions/sensor-actions';
import {
  takeDataMotor
} from '../redux-store/actions/motor-actions'
import {
  toast
} from 'react-toastify'

// Create connection via sockjs and stompjs
const sock = new SockJS(process.env.REACT_APP_API_ROOT + '/stomp');
const stompClient = Stomp.over(sock);
let lowerMotor = -1;
let upperMotor = -1;
// On connected
stompClient.connect({}, frame => {
  console.log("connected", frame);

  stompClient.subscribe("/topic/info", data => {
    console.log("receive", data);
  });

  stompClient.subscribe("/topic/sensor", data => {
    let sensorData = JSON.parse(data.body);
    let current = sensorData.sensor.currentValue

    if (current < lowerMotor && lowerMotor !== -1) {
      toast.warning("Sensor " + sensorData.sensor.deviceId + " có giá trị cảm biến hiện tại: " + current + ". Dưới ngưỡng cho phép.");
    } else if (current > upperMotor && upperMotor !== -1) {
      toast.warning("Sensor " + sensorData.sensor.deviceId + " có giá trị cảm biến hiện tại: " + current + ". Vượt ngưỡng cho phép.");
    }

    console.log("sensor", sensorData);
    store.dispatch(takeDataSensor(sensorData));
  });

  stompClient.subscribe("/topic/motor/status", data => {
    let motorData = JSON.parse(data.body);
    console.log("abc", motorData.motor);
    store.dispatch(takeDataMotor(motorData));
    lowerMotor = motorData.motor.lowerSensorBound;
    upperMotor = motorData.motor.upperSensorBound
  });

  // controlMotor('Speaker', 1000);
});

function send(dataObject, topic) {
  stompClient.send(topic, {}, JSON.stringify(dataObject));
}

export function controlMotor(deviceId, value) {
  send({
    deviceId,
    value
  }, '/app/motor/control');
}

export default stompClient;