import React from 'react';
import SensorDetailRow from './SensorDetailRow.js';
import { Link } from 'react-router-dom';
import axios from '../../utils/axios-instance';

import CanvasJSReact from './canvasjs.react';
//var CanvasJSReact = require('./canvasjs.react');
// var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

var timer;

class SensorDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      name: this.props.name,
      data: [
      ],
      currentValue: 0,
      everageWeek: 0,
      everageMonth: 0
    }
    this.setDataToState = this.setDataToState.bind(this);
    this.considerState = this.considerState.bind(this);
  }
  
  considerState = (value) => {
    if (value >= 700) {
      return "Ẩm";
    }
    if (value >= 450 && value < 700) {
      return "Bình thường";
    }
    return "Khô";
  }


  setDataToState = (data) => {
    // console.log(data);
    function addDays(dateObj, numDays) {
      dateObj.setDate(dateObj.getDate() + numDays);
      return dateObj;
    }  
    var sumWeek = 0;
    var lengthWeek = 0;
    var sumMonth = 0;
    var lengthMonth = 0;
    var _data = data
      .filter(detail => detail.sensor.deviceId === this.state.name)
      .map(detail => {
        if (new Date(detail.publishTime) > addDays(new Date(),-30)){
          lengthMonth +=1;
          sumMonth += detail.value;
          if (new Date(detail.publishTime) > addDays(new Date(),-7)){
            lengthWeek +=1;
            sumWeek += detail.value;
          }
        }
        return {
          x: new Date(detail.publishTime),
          y: detail.value
        }
      })
    this.setState({
      data: _data,
      everageWeek: Math.round(sumWeek/lengthWeek),
      everageMonth: Math.round(sumMonth/lengthMonth)
    })
    // console.log(_data);
  }

  componentDidMount() {
    axios.get('/sensordetail/list')
      .then(data => {
        // console.log(data);
        for (var i = 0; i < data.data.length; i++) {
          if (data.data[i].sensor.deviceId === this.state.name) {
            this.setState({ currentValue: data.data[i].sensor.currentValue });
          }
        }
        this.setDataToState(data.data);

      }).catch(error => {
        console.log(error);
      });
    timer = setInterval(() => axios.get('/sensordetail/list')
      .then(data => {
        // console.log(data);
        for (var i = 0; i < data.data.length; i++) {
          if (data.data[i].sensor.deviceId === this.state.name) {
            this.setState({ currentValue: data.data[i].sensor.currentValue });
          }
        }
        this.setDataToState(data.data);
      }).catch(error => {
        console.log(error);
      }), 5000)

  }

  componentWillUnmount(){
    clearInterval(timer);
  }

  render() {
    const options = {
      animationEnabled: false,
      title: {
        text: "Sensor value Chart"
      },
      axisX: {
        title: "Time",
        interval: 2,
        valueFormatString: "HH:mm",
      },
      axisY: {
        title: "Value",
        includeZero: false

      },
      data: [
        {
          type: "line",
          dataPoints: this.state.data
        }
      ]
    }
    return (
      <div className="list-sensor-info">
        <div className="row">
          <div className='col-2'>
            <Link id="turn-back" className="btn btn-outline-info" to={'/app/sensor'}>Quay về</Link>
          </div>
          <div className='col-8'>
            <h1>Cảm biến {this.props.name}</h1>
          </div>  
        </div>
        <div className="row"> 
          <div className="col-4 sensor-value-table">
            <table className="table table-bordered center">
              <tbody>
                <tr>
                  <th>Số đo hiện tại</th>
                  <td>{this.state.currentValue}</td>
                </tr>
                <tr>
                  <th>Số đo trung bình tuần</th>
                  <td>{this.state.everageWeek}</td>
                </tr>
                <tr>
                  <th>Số đo trung bình tháng</th>
                  <td>{this.state.everageMonth}</td>
                </tr>
              </tbody>
            </table>
          </div>   
          {/* <h4>Số đo hiện tại: {this.state.currentValue}</h4>
          <h4>Số đo trung bình tuần: {this.state.everageWeek}</h4>
          <h4>Số đo trung bình tháng: {this.state.everageMonth}</h4> */}
          <div className="col-8">
            <CanvasJSChart options={options}></CanvasJSChart>
          </div>
        </div>
        <div className="value-list-table">
          <table className="table table-hover table-detail ">
            <thead className="thead-light">
              <tr>
                <th>Thời gian</th>
                <th>Số đo</th>
                <th>Trạng thái</th>
              </tr>
            </thead>
            <tbody>
              {
                this.state.data.reverse().map(data => {
                  return (<SensorDetailRow key={data.x} time={data.x} measure={data.y} state={this.considerState}></SensorDetailRow>);
                })
              }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default SensorDetail;
