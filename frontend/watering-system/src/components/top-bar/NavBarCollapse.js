import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

// Redux actions
import { toggleSidebar } from '../../redux-store/actions/ui-actions';

// Style
import style from './topbar.module.scss';

// Utils
import { getClassName } from '../../utils/component-utils';

function NavbarCollapse(props) {
  let adminPageLink = null;
  let historyPageLink =null;
  if (props.role === 'ROLE_ADMIN') {
    adminPageLink =
      <li className="nav-item" onClick={props.toggleSidebar}>
        <Link to="/app/admin" className="nav-link">Admin</Link>
      </li>;
    historyPageLink = 
    <li className="nav-item" onClick={props.toggleSidebar}>
    <Link to="/app/historyadmin" className="nav-link">Lịch sử</Link>
  </li>;
  }

  let status = '';

  if (props.active === true) {
    status = style.Active;
  } else if (props.active === false && props.firstRender === false) {
    status = style.Inactive;
  }

  return (
    <div className={getClassName("collapse navbar-collapse mr-auto", style.MyNavBarCollapse, status)}>
      <ul className="navbar-nav mr-auto mt-0">
        <li className="nav-item active" onClick={props.toggleSidebar}>
          <Link to="/app" className="nav-link">Trang chủ</Link>
        </li>
        <li className="nav-item" onClick={props.toggleSidebar}>
          <Link to="/app/sensor" className="nav-link">Cảm biến</Link>
        </li>
        <li className="nav-item" onClick={props.toggleSidebar}>
          <Link to="/app/motor" className="nav-link">Máy bơm</Link>
        </li>
        {adminPageLink}
        {historyPageLink}
      </ul>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    active: state.ui.sideBarActive,
    firstRender: state.ui.sideBarFirstRender,
    role: state.user.role
  }
}

function mapDispatchToProps(dispatch) {
  return {
    toggleSidebar: () => {
      if (window.innerWidth < 768) {
        dispatch(toggleSidebar())
      }
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavbarCollapse);
