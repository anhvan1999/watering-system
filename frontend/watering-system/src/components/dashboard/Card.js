import React from 'react';
import { useHistory } from 'react-router-dom';
import style from "./dashboard.module.scss";

function Card(props) {
  let history = useHistory();

  let clickHandle = () => {
    history.push(props.dest);
  };

  return (
    <div className={"col-md mt-4 " + style.Card} onClick={clickHandle}>
      <div className={"w-100 h-100 d-flex align-items-center " + style.Box}>
        <h2 className="text-light text-center d-block w-100">{props.name}</h2>
      </div>
    </div>
  );
}

export default Card;
