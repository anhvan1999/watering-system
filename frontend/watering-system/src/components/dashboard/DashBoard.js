import React from 'react';
import style from './dashboard.module.scss';
import Card from './Card';

function DashBoard(props) {
  return (
    <div className={"container-fluid padding-top-62 d-flex align-items-center justify-content-center " + style.DashBoard}>
      <div className="row w-100 pt-sm-4 pt-md-0 mt-sm-4 mt-md-0">
        <Card name="Quản lí cảm biến" dest="/app/sensor"></Card>
        <Card name="Quản lý máy bơm" dest="/app/motor"></Card>
        <Card name="Thông tin cá nhân" dest="/app/user"></Card>
      </div>
    </div>
  );
}

export default DashBoard;
