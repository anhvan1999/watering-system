import React from 'react';
import TopBar from '../top-bar/TopBar';
import './HistoryListInfo.scss';
import { connect } from 'react-redux';
import { FaHistory } from 'react-icons/fa';

import axios from '../../utils/axios-instance';
var timer;

class HistoryList extends React.Component {
   
  constructor(props){
    super(props);
    this.state = {
        historylist : [],
    }
}

  componentDidMount () {
    console.log(this.props);
    axios.get('/history/all', {
        headers: {
            'Authorization': `jwt ${this.props.token}`
        }
    }).then(res => {
        
      this.setState({historylist: res.data});
    }).catch(error => {
        console.log(error);
    });
    timer = setInterval(() => axios.get('/history/all', {
        headers: {
            'Authorization': `jwt ${this.props.token}`
        }
    })
        .then(res => {
            
            this.setState({historylist: res.data});
        }).catch(error => {
            console.log(error);
        }), 5000)
}

componentWillUnmount(){
    clearInterval(timer);
}
  render(){
   
  return (
    <div id="list-info-history">
      <TopBar></TopBar>
      {
        <div className="infohistory">
          <div className="card-header">
              
            <h3 className="text-center text-primary"><FaHistory></FaHistory> Lịch sử tất cả người dùng</h3>
          </div>

          <br></br>
          <div className="card-body">

            <div className="table-resonsive">
              <table className="Table Table-striped">
                <thead className="thead-dark">
                  <tr>
                    <th>Họ tên</th>
                    <th>Thao tác</th>
                    <th>Thời gian </th>
                  </tr>
                </thead>
                <tbody>   
                  {
                    this.state.historylist.map((user,index)=> {
                      return (<tr key={index}>
                      <td> {user.username}</td>
                      <td> {user.action}</td>
                      <td> {user.time}</td>
                    </tr>)
                    })
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>

      }


    </div>
  

  );
    }
}
function mapStateToProps(state) {
  let result = {
      token: state.user.jwtToken,
      username: state.user.username
  };
  console.log("state",state);
  return result;
}

export default connect(mapStateToProps,null)(HistoryList);
