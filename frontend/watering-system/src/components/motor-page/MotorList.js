import React from 'react';
import MotorLabel from './MotorLabel';
import axios from '../../utils/axios-instance';
import { connect } from 'react-redux';
import {takeDataMotor} from '../../redux-store/actions/motor-actions';

class MotorList extends React.Component {
  componentDidMount() {
    axios.get('/motor/list', {
        headers: {
            'Authorization': `jwt ${this.props.token}`
        }
    }).then(res => {
      console.log(res);
      let data = res.data;
      for (let item of data) {
        console.log(item);
        this.props.setList({
          motor:{
            currentValue: item.currentValue,
            deviceId: item.deviceId
          }, 
          motorid: item.deviceId
        })
      }
      console.log(this.props);
    }).catch(error => {
        console.log(error);
    });
}

  render(){
    return (
      <div className="MotorList ">
      <table className='table table-hover motorList-table'>
        <thead className='thead-light'>
          <tr>
            <th>Máy bơm</th>
            <th>Giá trị</th>
            <th>Chi tiết</th>
          </tr>
        </thead>
        <tbody>
          {this.props.motor.map(x =>(
            <MotorLabel key={x.motorid} name={x.motorid} value={x.data.motor.currentValue} clicktoDetail={this.props.clicktoDetail}></MotorLabel>
          ))}
        </tbody>
      </table>
      </div>
    );
  }  
}

function mapStateToProps(state) {
  let result = {
      motor: state.motor,
      token: state.user.jwtToken
  };
  return result;
}
function mapDispatchToProps(dispatch) {
  return {
      setList: (data) => { dispatch(takeDataMotor(data)); }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MotorList);
