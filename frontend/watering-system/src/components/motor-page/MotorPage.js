import React from 'react';
import MotorList from './MotorList';
import './motorpage.scss';
import axiosInstance from '../../utils/axios-instance';
import axios from '../../utils/axios-instance';
import { connect } from 'react-redux';
import {takeDataMotor} from '../../redux-store/actions/motor-actions';

class MotorPage extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        motorName:'',
        newMotor:''
    }
    this.setIdDetail = this.setIdDetail.bind(this);
    this.setNewMotor = this.setNewMotor.bind(this);
    this.addMotor = this.addMotor.bind(this);
  }


  setIdDetail = (name) =>{
    this.setState({motorName:name})
  }
  setNewMotor(event){
    this.setState({newMotor: event.target.value})
  }

  addMotor(event){
    if (this.state.newMotor === ""){
      alert('Nhập tên máy bơm');
    }
    else{
      axios.post("/motor/add", this.state.newMotor,{
        headers: {
          'Authorization': `jwt ${this.props.token}`,
        }
      }).then(res =>{
        console.log(res.data);
        if (!res.data){
          alert('Tên máy bơm đã tồn tại');
        }
        else {
          let eventNameHistory = "Thêm mới motor " + this.state.newMotor;
          axios.post('/history/add', { eventName: eventNameHistory, deviceId: this.state.newMotor }, {
              headers: {
                  'Authorization': `jwt ${this.props.token}`,
              }
          }).then(() => {
            
          }).catch(error => {
              console.log(error);
          });
          alert("Thêm thành công");
          this.props.setList({
            motor:{
              currentValue: res.data.currentValue,
              deviceId: res.data.deviceId
            }, 
            motorid: res.data.deviceId
          })
        }
      }).catch(error =>{
        console.log(error);
      });
    }
  }

  render(){
    return (
            <div className="container-fluid MotorPage">
              <h1>Thông tin và điều khiển máy bơm</h1>
              <div className='add-motor'>
                <div className="text-center row">
                  <div className='col-4'>
                    <p>Nhập tên máy bơm</p>
                  </div>
                  <div className='col-6'>
                    <input
                          required
                          type="text"
                          className="form-control" onChange={this.setNewMotor}/>
                  </div>
                  <div className='col-2'>
                    <button className="btn btn-outline-info" onClick={this.addMotor}>Thêm</button>
                  </div>
                </div>
              </div>
              <MotorList clicktoDetail={this.setIdDetail}></MotorList>
            </div>
          
    );
  }
}
function mapStateToProps(state) {
  let result = {
      token: state.user.jwtToken
  };
  return result;
}

function mapDispatchToProps(dispatch) {
  return {
      setList: (data) => { dispatch(takeDataMotor(data)); }
  };
}

export default connect(mapStateToProps,mapDispatchToProps) (MotorPage);
