import React from 'react';
// import { connect } from 'react-redux';
import axios from '../../utils/axios-instance';
import style from './motordetail.module.scss';
import { Link } from 'react-router-dom';
import { getClassName } from '../../utils/component-utils';
import { connect } from 'react-redux';
import { controlMotor } from '../../service/ws-service';
import MotorDetailRow from './MotorDetailRow';
import MotorHistoryRow from './MotorHistoryRow';
import CanvasJSReact from '../sensor-page/canvasjs.react';

var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var timer;
var timer_second;
class MotorDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentStatus: <span></span>,
            uppercurrentStatus: 0,
            lowercurrentStatus: 0,
            data: [],
            currentValue: 0,
            inputwater: 0,
            deviceId: this.props.name,
            uppersubmit: 0,
            lowersubmit: 0,
            datachart: [],
            dataHistory: [],
            everageWeek: 0,
            everageMonth: 0,
            preStatus:[0,1024],
            isHandle:false,
        }

    }


    checkStatus = (data) => {
        if (data === 0) {
            return <span className="badge badge-danger">Tắt</span>;
        }
        else {
            return <span className="badge badge-primary">Bật</span>;
        }
    }
    checkhandcontrol() {
        if (document.getElementById("controlhand").value === 'On') {
            this.setState({
                isHandle:true,
                preStatus:[this.state.lowercurrentStatus,this.state.uppercurrentStatus],
            });
            let data = { function: "DEFAULT", lower:0,upper:1024, id: this.state.deviceId };
            axios.post('/motor/control', data, {
                headers: {
                    'Authorization': `jwt ${this.props.token}`,
                }
            }).then(() => {
                this.setState({
                    lowercurrentStatus: 0,
                    uppercurrentStatus: 1024,
                });
            }).catch(error => {
                console.log(error);
            });
            document.getElementById("controlhand").value = 'Off';
            document.getElementById("controlhand").innerHTML = 'Tắt';
            document.getElementById("forminputwater").style.visibility = 'visible';
        }
        else {
            document.getElementById("controlhand").value = 'On';
            document.getElementById("controlhand").innerHTML = 'Bật';
            document.getElementById("forminputwater").style.visibility = 'hidden';
            this.setState({
                isHandle:false,
                lowercurrentStatus: this.state.preStatus[0],
                uppercurrentStatus: this.state.preStatus[1],
            }); 
            controlMotor(this.state.deviceId, 0);
            let data = { function: "DEFAULT", lower:this.state.preStatus[0],upper:this.state.preStatus[1], id: this.state.deviceId};
            axios.post('/motor/control', data, {
                headers: {
                    'Authorization': `jwt ${this.props.token}`,
                }
            }).then(() => {
                this.setState({
                    currentValue: 0,
                    currentStatus :this.checkStatus(0),
                });
            }).catch(error => {
                console.log(error);
            }); 
           
        }
    }
    controlwater(e) {
        if (e.target.value === '') {
            alert("Vui lòng nhập giá trị");
        }
        else
            this.setState({ inputwater: parseInt(e.target.value )});
    }
    submitwater(e) {
        if (this.state.inputwater < 0 || this.state.inputwater > 5000) {
            alert("Vui lòng nhập lại giá trị nước cần bơm hợp lệ!");
        }
        // else if (this.state.currentValue > this.state.uppercurrentStatus || this.state.currentValue < this.state.lowercurrentStatus) {
        //     alert("Giá trị hiện tại vượt quá ngưỡng cho phép");
        //     document.getElementById("controlhand").value = 'On';
        //     document.getElementById("controlhand").innerHTML = 'Bật';
        //     document.getElementById("forminputwater").style.visibility = 'hidden';
        // }
        else {
            controlMotor(this.state.deviceId, this.state.inputwater);
            let eventNameHistory = "Đặt giá trị " + this.state.inputwater + " cho " + this.state.deviceId;
            axios.post('/history/add', { eventName: eventNameHistory, deviceId: this.state.deviceId }, {
                headers: {
                    'Authorization': `jwt ${this.props.token}`,
                }
            }).then(() => {

            }).catch(error => {
                console.log(error);
            });
            alert("Bạn đã bơm nước thành công");
        }
    }
    setlowersubmit(e) {
        if (e.target.value === '') {
            alert("Vui lòng nhập giá trị");
        }
        else {
            this.setState({ lowersubmit:  parseInt(e.target.value) });
        }
    }
    setuppersubmit(e) {
        if (e.target.value === '') {
            alert("Vui lòng nhập giá trị");
        }
        else {
            this.setState({ uppersubmit: parseInt(e.target.value) });
        }
    }
    submitlower(e) {
        if (this.state.lowersubmit >= 0 && this.state.lowersubmit <= 5000 && this.state.lowersubmit < this.state.uppercurrentStatus) {
            let data = { function: "LOWER", lower: this.state.lowersubmit,upper:this.state.uppercurrentStatus, id: this.state.deviceId };
            axios.post('/motor/control', data, {
                headers: {
                    'Authorization': `jwt ${this.props.token}`,
                }
            }).then(() => {
                this.setState({
                    lowercurrentStatus: this.state.lowersubmit
                });
            }).catch(error => {
                console.log(error);
            });
            let eventNameHistory = "Thiết lập ngưỡng dưới với giá trị " + this.state.lowersubmit + " cho " + this.state.deviceId;
            axios.post('/history/add', { eventName: eventNameHistory, deviceId: this.state.deviceId }, {
                headers: {
                    'Authorization': `jwt ${this.props.token}`,
                }
            }).then(() => {

            }).catch(error => {
                console.log(error);
            });
            alert("Bạn đã gửi cho server thành công!");

        }
        else {
            alert("Ngưỡng có giá trị từ 0 đến 5000 và ngưỡng thấp nhất phải thấp hơn ngưỡng cao nhất!");
        }
    }

    submitupper(e) {
        if (this.state.uppersubmit >= 0 && this.state.uppersubmit <= 5000 && this.state.uppersubmit > this.state.lowercurrentStatus) {
            let data = { function: "UPPER",lower:this.state.lowercurrentStatus, upper: this.state.uppersubmit, id: this.state.deviceId };
            axios.post('/motor/control', data, {
                headers: {
                    'Authorization': `jwt ${this.props.token}`,
                }
            }).then(() => {
                this.setState({
                    uppercurrentStatus: this.state.uppersubmit
                });
            }).catch(error => {
                console.log(error);
            });
            let eventNameHistory = "Thiết lập ngưỡng trên với giá trị " + this.state.uppersubmit + " cho " + this.state.deviceId;
            axios.post('/history/add', { eventName: eventNameHistory, deviceId: this.state.deviceId }, {
                headers: {
                    'Authorization': `jwt ${this.props.token}`,
                }
            }).then(() => {

            }).catch(error => {
                console.log(error);
            });
            alert("Bạn đã gửi cho server thành công!");
        }
        else {
            alert("Ngưỡng có giá trị từ 0 đến 5000 và ngưỡng cao nhất phải cao hơn ngưỡng thấp nhất !");
        }
    }

    setDataDefault = (data) => {
        console.log(data);
        let last_index = data.data.length - 1;
        if (data.data[last_index].deviceId === this.state.deviceId) {
            // var _data = this.state.datachart.concat({
            //     x: new Date(data.data[last_index].publishTime),
            //     y: data.data[last_index].motor.currentValue
            // });
            this.setState({
                currentValue: data.data[last_index].currentValue,
                uppercurrentStatus: data.data[last_index].upperSensorBound,
                lowercurrentStatus: data.data[last_index].lowerSensorBound,
                currentStatus: this.checkStatus(data.data[last_index].currentValue),
            });
        }
    }
    setData = (data) => {
        console.log(data);
        let last_index = data.data.length - 1;
        if (data.data[last_index].motor.deviceId === this.state.deviceId) {

            var _data = this.state.datachart.concat({
                x: new Date(data.data[last_index].publishTime),
                y: data.data[last_index].motor.currentValue
            });
            this.setState({
                currentValue: data.data[last_index].motor.currentValue,
                uppercurrentStatus: data.data[last_index].motor.upperSensorBound,
                lowercurrentStatus: data.data[last_index].motor.lowerSensorBound,
                currentStatus: this.checkStatus(data.data[last_index].motor.currentValue),
                datachart: _data
            });
        }
    }
    setDataToState = (data) => {
        console.log(data);
        function addDays(dateObj, numDays) {
            dateObj.setDate(dateObj.getDate() + numDays);
            return dateObj;
        }
        let sumWeek = 0;
        let lengthWeek = 0;
        let sumMonth = 0;
        let lengthMonth = 0;
        let _data = data
            .filter(detail => detail.motor.deviceId === this.state.deviceId)
            .map(detail => {
                if (new Date(detail.publishTime) > addDays(new Date(), -30)) {
                    lengthMonth += 1;
                    sumMonth += detail.status;
                    if (new Date(detail.publishTime) > addDays(new Date(), -7)) {
                        lengthWeek += 1;
                        sumWeek += detail.status;
                    }
                }
                return {
                    id: detail.id,
                    time: new Date(detail.publishTime),
                    value: detail.status,
                    lower: detail.motor.lowerSensorBound,
                    upper: detail.motor.upperSensorBound
                }
            });
        this.setState({
            data: _data,
            everageWeek: Math.round(sumWeek / lengthWeek),
            everageMonth: Math.round(sumMonth / lengthMonth)
        });
    }
    componentWillUnmount() {
        clearInterval(timer);
        clearInterval(timer_second);
    }
    getMotorDetail = (link, isSecond) => {
        axios.post(link, { deviceId: this.state.deviceId }, {
            headers: {
                'Authorization': `jwt ${this.props.token}`
            },
        }).then(data => {
            console.log(data);
            if (isSecond) {
                if (data.data.length > 0) {
                    this.setData(data);
                    this.setDataToState(data.data);
                }
            }
            else {
                this.setDataDefault(data);
            }

        }).catch(error => {
            console.log(error);
        });
    }
    setDataHistory = (data) => {
        let d = data.data;
        console.log(d);
        if (d.length > 0) {
            this.setState({
                dataHistory: d
            });
        }
    }
    getHistoryMotorDetail = () => {
        axios.post('/history/listMotorHistory', { deviceId: this.state.deviceId }, {
            headers: {
                'Authorization': `jwt ${this.props.token}`
            }
        }).then(data => {
            this.setDataHistory(data);
        }).catch(error => {
            console.log(error);
        });
    }
    componentDidMount() {
        this.getMotorDetail('/motor/getMotorDefault', false);
        // this.getHistoryMotorDetail();
        timer = setInterval(() => this.getMotorDetail('/motordetail/listValue', true), 2000)
        timer_second = setInterval(() => this.getHistoryMotorDetail(), 2000);
    }

    callback = (key) => {
        console.log(key);
    }
    render() {
        const options = {
            animationEnabled: false,
            title: {
                text: "Biểu đồ lượng nước máy bơm"
            },
            axisX: {
                title: "Thời gian",
                interval: 2000,
                valueFormatString: "HH:mm",
            },
            axisY: {
                title: "Lượng nước",
                includeZero: true

            },
            data: [
                {
                    type: "line",
                    dataPoints: this.state.datachart
                }
            ]
        }

        return (
            <div className={getClassName(style.MotorInfo)}>

                <div className={getClassName(style.ListMotorInfo, "container-fluid")}>

                    <div className={getClassName("row", style.MotorTitle)}>
                        <div className="col-2 d-flex justify-content-center align-items-center">
                            <Link id="turn-back" className='btn btn-outline-info btn-motordetail' to={'/app/motor'}>Quay về</Link>
                        </div>
                        <div className="col-8">
                            <h1>Máy bơm {this.props.name}</h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className={getClassName("col-lg-12", style.RowSettingMargin)}>
                            <div className="row">
                                <div className={getClassName("col-lg-3")}>
                                    <div className={style.MotorStatusContainer}>
                                        <table className="table table-bordered center">
                                            <tbody>
                                                <tr>
                                                    <th scope="row">Trạng thái hiện tại</th>
                                                    <td>{this.state.currentStatus}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Số đo hiện tại</th>
                                                    <td> {this.state.currentValue}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Giá trị ngưỡng thấp nhất</th>
                                                    <td>{this.state.lowercurrentStatus}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Giá trị ngưỡng cao nhất </th>
                                                    <td>{this.state.uppercurrentStatus}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Giá trị trung bình tuần </th>
                                                    <td>{this.state.everageWeek}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Giá trị trung bình tháng </th>
                                                    <td>{this.state.everageMonth}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className={getClassName("col-lg-5", style.MotorSettingContainer)}>
                                    <div className={getClassName(style.MotorStatusPaddingTop)}>
                                        <div className={getClassName("row col-12 d-flex justify-content-center align-items-center")}>
                                            <div className="col-5 ">Nhập giá trị ngưỡng thấp nhất </div>

                                            <div className="col-5">
                                                <input
                                                    required
                                                    type="number"
                                                    className={getClassName("form-control", style.MotorForm)}
                                                    onChange={this.setlowersubmit.bind(this)} />

                                            </div>
                                            <div className={getClassName("col-2")}>
                                                <button className="btn btn-outline-info"

                                                    onClick={this.submitlower.bind(this)}> Gửi </button>
                                            </div>

                                        </div>
                                        <br></br>
                                        <div className={getClassName("row col-12 d-flex justify-content-center align-items-center")}>
                                            <div className="col-5">Nhập giá trị ngưỡng cao nhất </div>
                                            <div className="col-5">
                                                <input
                                                    required
                                                    type="number"
                                                    className={getClassName("form-control", style.MotorForm)}
                                                    onChange={this.setuppersubmit.bind(this)} />

                                            </div>
                                            <div className={getClassName("col-2")}>
                                                <button className="btn btn-outline-info"
                                                    onClick={this.submitupper.bind(this)}> Gửi </button>
                                            </div>

                                        </div>

                                        <br></br>
                                        <p className="col-12" >Bật/tắt chế độ điều khiển bằng tay: <button id="controlhand" className="btn btn-primary btn-sensor" value="On"
                                            onClick={this.checkhandcontrol.bind(this)}> Bật</button></p>
                                        <div id="forminputwater" className={getClassName("form-group row", style.MotorControlHand)}>
                                            <div className={getClassName("row col-12 d-flex justify-content-center align-items-center")}>
                                                <div className="col-7">Nhập cường độ máy bơm : </div>
                                                <div className="col-5">
                                                    <input
                                                        required
                                                        type="number"
                                                        className={getClassName("form-control", style.MotorForm
                                                        )}
                                                        id="inputwater"
                                                        onChange={this.controlwater.bind(this)} />
                                                </div>
                                                <div className={getClassName("col-12", style.ButtonPadding)}>
                                                    <button id="submitwater" className="btn btn-outline-info" onClick={this.submitwater.bind(this)}> Kích hoạt </button>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div className={getClassName("col-lg-4")}>

                                    <div className={style.MotorGroupContainer}>
                                        <table className="table table-hover">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th>Thời gian</th>
                                                    <th>Người dùng</th>
                                                    <th>Thao tác</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.dataHistory.reverse().map(data1 => {
                                                        return (<MotorHistoryRow key={data1.id} time={data1.time} action={data1.action} username={data1.username}></MotorHistoryRow>);
                                                    })
                                                }
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div className={getClassName("col-lg-12", style.RowSettingMargin)}>
                            <div className="row">
                                <div className="col-lg-8">
                                    <div className={getClassName(style.MotorStatus, style.MotorSettingContainer)}>
                                        <CanvasJSChart options={options}></CanvasJSChart>
                                    </div>
                                </div>
                                <div className={getClassName("col-lg-4")}>
                                    <div className={style.MotorGroupContainer}>
                                        <table className="table table-hover">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th>Thời gian</th>
                                                    <th>Giá trị</th>
                                                    <th>Ngưỡng dưới</th>
                                                    <th>Ngưỡng trên</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.data.reverse().map(data => {
                                                        return (<MotorDetailRow key={data.id} time={data.time} value={data.value} upper={data.upper} lower={data.lower}></MotorDetailRow>);
                                                    })
                                                }
                                            </tbody>
                                        </table>

                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>



                </div>

            </div>
        );
    }
}
function mapStateToProps(state) {
    let result = {
        token: state.user.jwtToken
    };
    return result;
}
export default connect(mapStateToProps, null)(MotorDetail)
