import React from 'react';
export default function MotorHistoryRow(props) {
    return (
        <tr>
            <td>{props.time}</td>
            <td>{props.username}</td>                     
            <td>{props.action}</td>       
        </tr>
    )
}
